+++
title = "About"
date = "2014-05-18"
menu = "main"
+++

I'm a software developer from Reykjavík, Iceland. During the daytime I write software for the banking industry.

### This page

This site is built using [Hugo](http://github.com/spf13/hugo) and is using a modified version of the [Hyde](https://github.com/spf13/hugoThemes/tree/master/themes/hyde) which spf13 ported over to Hugo. Social icons on the sidebar are from [Socicon](http://socicon.com/). Syntax highlighting provided by [highlight.js](http://highlightjs.org/).