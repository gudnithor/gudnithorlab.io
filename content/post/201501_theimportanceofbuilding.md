+++
title = "The importance of building"
date = "2015-01-26"
draft = false
+++

It's now the 26th of January and I'm plugging my trusty laptop to my charger for the first time this year. Not a really painful new years resolution to cut down on computer time; just caused by not having enough time to spend wandering around the internet.

In a rather eventful IKEA trip around christmas my significant other and me decided to start renovate the bathroom in our small apartment. What seemed like a little more than two weekends work of getting rid of the old tiles and appliances turned out to be overwhelming amount of work after I had used up a whole weekend just getting rid of the old stuff. Just like in most software projects not only the estimated time was pretty far off, so was our budget. Both the planned time and the budget blew up by 2x.

After little more than three weeks of regular work, then doing renovation work in the bathroom and ending each day on a trip to my parents for a shower because our whole apartment was as dusty as an egyptian tomb I realized something. This is something I enjoy. Sure the dust and not having a shower every morning was annoying but trying to do as much of the work as I could by myself was rewarding. I'm lucky to have both my father and his brother to help me and provide guidance. 

I'm thankful for the opportunity to be able to take on projects I have not done before and sometimes way beyond my comfort zone. Both at work and at home. Next bathroom in my next apartment will be way quicker and better. I might even do the kitchen myself next time! Each project at work seems to be a lot easier than the next one before it and it seems to be better done as well.

Building it yourself means not only building the project at hand - you are also building your experience which is probably more valuable than the project. I'm looking forward to my next project whether it will be fixing my aging jeep or learning something new at work.